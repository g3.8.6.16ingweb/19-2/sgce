<?php

class Eventos extends CI_Controller{

    private $permisos;
    public function __construct(){
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->model('evento');
        $this->load->model('participante');
        $this->permisos=$this->backend_lib->control();
        $this->load->database();
        $this->load->library('form_validation');
    }

    public function index(){
        redirect("Eventos/listado");
    }

    public function buscar_listado( ){
        redirect("/eventos/listado/1?nombre=".$this->input->get("nombre"));
    }
    public function listado($pag=1){
        $pag--;
        if($pag<0){
            $pag=0;
        }
        $page_size=2;
        $offset=$pag*$page_size;
        $nombre = $this->input->get("nombre");

        //$vdata["eventos"]=$this->evento->findAll();
        

        $vdata["eventos"]=$this->evento->pagination($page_size,$offset,$nombre);
        $vdata["primerpagina"]=$pag+1;
        $vdata["nombre"]=$nombre;
        $vdata["finalpagina"]=ceil($this->evento->count($nombre)/$page_size);
        $vdata["permisos"]=$this->permisos;

        $this->load->view('Eventos/listadoEvento',$vdata);

    }
    public function guardar($evento_id=null){
        if(! $this->permisos->insertar){ redirect(base_url()); return; } 
        if(! $this->permisos->actualizar){ redirect(base_url()); return; } 
        $this->form_validation->set_rules('nombre','Nombre',array( 'alpha'));
        $this->form_validation->set_rules('tipo','Tipo',array('required', 'alpha'));
        $this->form_validation->set_rules('lugar','Lugar','required');
        $this->form_validation->set_rules('detalle','Detalle','required');
        $this->form_validation->set_rules('fechaInicio','Fecha_inicio','required');
        $this->form_validation->set_rules('fechaFin','Fecha_fin','required');
        $this->form_validation->set_rules('horaInicio','Hora_inicio','required');
        $this->form_validation->set_rules('horaFin','Hora_fin','required');

        $vdata["nombre"]= $vdata["tipo"]= $vdata["lugar"]= $vdata["detalle"]= $vdata["fecha_inicio"]= $vdata["fecha_fin"]= 
        $vdata["hora_inicio"]= $vdata["hora_fin"]= "";
        if(isset($evento_id)){
            $evento=$this->evento->find($evento_id);
            if(isset($evento)){
                
                $vdata["nombre"]= $evento->nombre;
                $vdata["tipo"]= $evento->tipo;
                $vdata["lugar"]= $evento->lugar;
                $vdata["detalle"]= $evento->detalle;
                $vdata["fecha_inicio"]= $evento->fecha_inicio;
                $vdata["fecha_fin"]= $evento->fecha_fin;
                $vdata["hora_inicio"]= $evento->hora_inicio;
                $vdata["hora_fin"]= $evento->hora_fin;
            }
        }

        if($this->input->server("REQUEST_METHOD")=="POST"){
                $data["nombre"]= $this->input->post("nombre");
                $data["tipo"]= $this->input->post("tipo");
                $data["lugar"]= $this->input->post("lugar");
                $data["detalle"]= $this->input->post("detalle");
                $data["fecha_inicio"]= $this->input->post("fechaInicio");
                $data["fecha_fin"]= $this->input->post("fechaFin");
                $data["hora_inicio"]= $this->input->post("horaInicio");
                $data["hora_fin"]= $this->input->post("horaFin");

                $vdata["nombre"]= $this->input->post("nombre");
                $vdata["tipo"]= $this->input->post("tipo");
                $vdata["lugar"]= $this->input->post("lugar");
                $vdata["detalle"]= $this->input->post("detalle");
                $vdata["fecha_inicio"]= $this->input->post("fechaInicio");
                $vdata["fecha_fin"]= $this->input->post("fechaFin");
                $vdata["hora_inicio"]= $this->input->post("horaInicio");
                $vdata["hora_fin"]= $this->input->post("horaFin");

            if($this->form_validation->run()){
                if(isset($evento_id)){
                    $this->evento->update($evento_id,$data);
                    redirect("Eventos/listado");
                }
                else{
                    $this->evento->insert($data);
                    redirect("Eventos/listado");
                }
            }

        }
        $this->load->view('Eventos/guardarEvento',$vdata);
    }

    public function borrar($evento_id = null){
        if(! $this->permisos->eliminar){ redirect(base_url()); return; } 
        $evento=$this->evento->delete($evento_id);
        redirect("Eventos/listado");
    }

    public function borrar_ajax($evento_id = null){
        if(! $this->permisos->eliminar){ redirect(base_url()); return; } 
        $evento=$this->evento->delete($evento_id);
        redirect("Eventos/listado");
    }

    public function ver($evento_id = null){
        $evento=$this->evento->find($evento_id);
        $adata=$this->evento->findAs($evento_id);
        if(isset($evento)){

            $vdata["id"]= $evento_id;
            $vdata["nombre"]= $evento->nombre;
            $vdata["tipo"]= $evento->tipo;
            $vdata["lugar"]= $evento->lugar;
            $vdata["detalle"]= $evento->detalle;
            $vdata["fecha_inicio"]= $evento->fecha_inicio;
            $vdata["fecha_fin"]= $evento->fecha_fin;
            $vdata["hora_inicio"]= $evento->hora_inicio;
            $vdata["hora_fin"]= $evento->hora_fin;
            $vdata["lista_participante"]= $this->evento->findAs($evento_id);
            $vdata["participantes"]=$this->participante->findAll();;
        }
        $this->load->view('Eventos/detalleEvento',$vdata);
    }
}