<?php

class Autenticacion extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('usuario');
        $this->load->database();
    }
    public function index(){
        $this->load->view("Login");
    }

    public function login(){
        $username = $this->input->post("username");
        $password = $this->input->post("password");
        $res = $this->usuario->login($username,$password);
        if(!$res){
            //$this->session->set_flashdate("error","El usuario y/o contraseña son incorrectos");
            redirect(base_url()."autenticacion");
        }
        else{
            $data = array(
                'id'=>$res->id,
                'nombre'=>$res->nombres,
                'rol'=>$res->rol_id,
                'login'=>1
            );
            $this->session->set_userdata($data);
            redirect(base_url());
        }
    }

    public function logout(){
        $this->session->sess_destroy();
        redirect(base_url()."eventos");
    }


}