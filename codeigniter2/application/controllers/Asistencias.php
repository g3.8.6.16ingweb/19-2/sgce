<?php

class Asistencias extends CI_Controller{

    public function __construct(){
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->model('participante');
        $this->load->model('asistencia');
        $this->load->model('estudiante');
        $this->load->database();
        $this->load->library('form_validation');
    }
    
    public function borrar($as_id = null){
        $evento=$this->evento->delete($as_id);
        redirect("Eventos/ver");
    }

    public function Insertar(){
        $data["id_participante"]=$this->input->post("id_participante");
        $data["id_evento"]=$this->input->post("id_evento");
        $creditos=$this->input->post("creditos");
        $res=$this->asistencia->find($data["id_participante"],$data["id_evento"]);
        $tes=$this->participante->find($data["id_participante"]);
        $cre=$this->estudiante->find($data["id_participante"]);
        $bdata["creditos"]=$cre->creditos + $creditos;
        if(!$res && $tes){
            $this->asistencia->insert($data);
            if($cre){
                $this->estudiante->update($data["id_participante"],$bdata);}
            redirect(base_url()."eventos/ver/".$data["id_evento"]);
        }
        redirect(base_url()."eventos");
    }

    public function buscar_listado( ){
        redirect("/asistencias/listado/1?nombre=".$this->input->get("nombre"));
    }

    public function listado($pag=1){

        $pag--;
        if($pag<0){
            $pag=0;
        }
        $page_size=2;
        $offset=$pag*$page_size;
        $nombre = $this->input->get("nombre");

        //$vdata["eventos"]=$this->evento->findAll();
        

        $vdata["participantes"]=$this->participante->pagination($page_size,$offset,$nombre);
        $vdata["primerpagina"]=$pag+1;
        $vdata["nombre"]=$nombre;
        $vdata["finalpagina"]=ceil($this->participante->count($nombre)/$page_size);

        $this->load->view('asistencias/listadoAsistencia',$vdata);
    }

}