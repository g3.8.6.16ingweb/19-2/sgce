<?php

class Participantes extends CI_Controller{

    private $permisos;
    public function __construct(){
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->model('participante');
        $this->load->model('estudiante');
        $this->permisos=$this->backend_lib->control();
        $this->load->database();
        $this->load->library('form_validation');
    }

    public function index(){
        redirect("Participantes/listado");
    }

    public function buscar_listado( ){
        redirect("/participantes/listado/1?nombre=".$this->input->get("nombre"));
    }
    public function listado($pag=1){

        $pag--;
        if($pag<0){
            $pag=0;
        }
        $page_size=2;
        $offset=$pag*$page_size;
        $nombre = $this->input->get("nombre");

        //$vdata["eventos"]=$this->evento->findAll();
        

        $vdata["participantes"]=$this->participante->pagination($page_size,$offset,$nombre);
        $vdata["primerpagina"]=$pag+1;
        $vdata["nombre"]=$nombre;
        $vdata["finalpagina"]=ceil($this->participante->count($nombre)/$page_size);
        $vdata["permisos"]=$this->permisos;

        $this->load->view('Participantes/listadoParticipante',$vdata);
    }
    public function guardar($evento_id=null){
        if(! $this->permisos->insertar){ redirect(base_url()); return; } 
        if(! $this->permisos->actualizar){ redirect(base_url()); return; } 

        $this->form_validation->set_rules('idparticipante','id_participante',array('required', 'numeric'));
        $this->form_validation->set_rules('nombre','nombre',array('required', 'alpha'));
        $this->form_validation->set_rules('apellido','apellido','required');
        $this->form_validation->set_rules('correo','correo','required');
        $this->form_validation->set_rules('estudiante','estudiante','required');

        $vdata["id_participante"]= $vdata["nombre"]= $vdata["apellido"]= $vdata["correo"]= $vdata["estudiante"]= "";
        if(isset($evento_id)){
            $evento=$this->participante->find($evento_id);
            if(isset($evento)){
                
                $vdata["id_participante"]= $evento->id_participante;
                $vdata["nombre"]= $evento->nombre;
                $vdata["apellido"]= $evento->apellido;
                $vdata["correo"]= $evento->correo;
            }
        }

        if($this->input->server("REQUEST_METHOD")=="POST"){
                $data["id_participante"]= $this->input->post("idparticipante");
                $data["nombre"]= $this->input->post("nombre");
                $data["apellido"]= $this->input->post("apellido");
                $data["correo"]= $this->input->post("correo");
                $data["estudiante"]= $this->input->post("estudiante");

                
                $vdata["id_participante"]= $this->input->post("idparticipante");
                $vdata["nombre"]= $this->input->post("nombre");
                $vdata["apellido"]= $this->input->post("apellido");
                $vdata["correo"]= $this->input->post("correo");
                if($this->estudiante->find($data["id_participante"])){
                    $data["estudiante"]= "si";
                    $vdata["estudiante"]= "si";}
                else{
                    $data["estudiante"]= "no";
                    $vdata["estudiante"]= "no";
                }

            if($this->form_validation->run()){
                if(isset($evento_id)){
                    $this->participante->update($evento_id,$data);
                    redirect("Participantes/listado");
                }
                else{
                    $this->participante->insert($data);
                    redirect("Participantes/listado");
                }
            }

        }
        $this->load->view('participantes/guardarParticipante',$vdata);
    }
    public function borrar($participante_id = null){
        if(! $this->permisos->eliminar){ redirect(base_url()); return; } 
        $participante=$this->participante->delete($participante_id);
        redirect("Participantes/listado");

    }

    public function borrar_ajax($participante_id = null){
        if(! $this->permisos->eliminar){ redirect(base_url()); return; } 
        $participante=$this->participante->delete($participante_id);
        redirect("Participantes/listado");
    }
    public function ver($participante_id = null){
        $participante=$this->participante->find($participante_id);
        if(isset($participante)){
                
            $vdata["nombre"]= $participante->nombre;
            $vdata["tipo"]= $participante->tipo;
            $vdata["lugar"]= $participante->lugar;
            $vdata["detalle"]= $participante->detalle;
            $vdata["fecha_inicio"]= $participante->fecha_inicio;
            $vdata["fecha_fin"]= $participante->fecha_fin;
            $vdata["hora_inicio"]= $participante->hora_inicio;
            $vdata["hora_fin"]= $participante->hora_fin;
        }
        $this->load->view('Participantes/detalleParticipante',$vdata);
    }
}