<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org">
<head th:fragment="head">
<meta charset="UTF-8" />
<title> Documento sin titulo</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<header >
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<a class="navbar-brand" href="#">Creditos Extracurriculares</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarNav" aria-controls="navbarNav"
				aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav nav-pills">
					<li class="nav-item"><a class="nav-link" href="<?php echo base_url() ?>eventos/listado">Evento
							<span class="sr-only">(current)</span>
					</a></li>
					<li class="nav-item active"><a class="nav-link" href="<?php echo base_url() ?>participantes/listado">Participantes</a>
					</li>
				</ul>
			</div>
		</nav>
	</header>
	<div class="container-fluid">
    <h1 class="display-4">Participantes</h1>
    <br>
    <a href="<?php echo base_url() ?>participantes/guardar" class="btn btn-success">Crear</a>
	
	<form method="get" action="<?php echo base_url() ?>participantes/buscar_listado">
	<div class="input-group mt-2 mb-2">
		<input name="nombre" type="text" value="<?php echo $nombre ?>" class="form-control" placeholder="Buscar">
			<div class="input-group-append">
			<button class="btn btn-outline-secondary" type="submit">Enviar</button>
			</div>
		</div>
	</form>
	
    <br>
    <table class="table">
			<thead>
				<th scope="col">#</th>
				<th scope="col">Nombre</th>
				<th scope="col">Apellido</th>
				<th scope="col">correo</th>
				<th scope="col">estudiante</th>
				<th scope="col">Operacion</th>
            </thead>
            <thbody>
                <?php foreach ($participantes as $key => $e) :?>
                    <tr>
                        <th scope="row"><?php echo $e->id_participante?></th>
                        <td><?php echo $e->nombre?></td>
                        <td><?php echo $e->apellido?></td>
                        <td><?php echo $e->correo?></td>
                        <td><?php echo $e->estudiante?></td>
                        <td>
                            <button type="button" class="btn btn-outline-dark"><a href="<?php echo base_url() ?>participantes/ver/<?php echo $e->id_participante?>">Detalle  </button>
                        </td>

                <?php endforeach; ?>

			
    </table>
	<nav aria-label="Page navigation example">
		<ul class="pagination">
			<?php
			$anterior=$primerpagina-1; 
			$siguiente=$primerpagina+1; 
			if($anterior<=0){ $prev=1;} 
			if($siguiente>$finalpagina){ $siguiente=$finalpagina;}
			?>
			<li class="page-item"><a class="page-link" href="<?php echo base_url()."participantes/listado/".$anterior ?>?nombre=<?php echo $nombre ?>">Previous</a></li>
			
			<?php
			 	for($i=1;$i<=$finalpagina;$i++){?>
					<li class="page-item"><a class="page-link" href="<?php echo base_url()."participantes/listado/".$i ?>?nombre=<?php echo $nombre ?>"><?php echo $i ?></a></li>
			<?php	 }
			?>
			<li class="page-item"><a class="page-link" href="<?php echo base_url()."participantes/listado/".$siguiente ?>?nombre=<?php echo $nombre ?>">Next</a></li>
		</ul>
	</nav>
    </div>
	
</body>
</html>