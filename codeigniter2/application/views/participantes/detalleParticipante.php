<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org">
<head th:fragment="head">
<meta charset="UTF-8" />
<title> Documento sin titulo</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</head>
<body>
	<header >
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<a class="navbar-brand" href="#">Creditos Extracurriculares</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarNav" aria-controls="navbarNav"
				aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav nav-pills mr-auto">   
				    <?php if($this->session->userdata("login")){?>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Evento</a>
						<div class="dropdown-menu" aria-labelledby="dropdown01">
						<a class="dropdown-item" href="<?php echo base_url() ?>eventos/listado">Listado</a>
						<a class="dropdown-item" href="<?php echo base_url() ?>eventos/guardar">Crear</a>
						</div>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Participantes</a>
						<div class="dropdown-menu" aria-labelledby="dropdown01">
						<a class="dropdown-item" href="<?php echo base_url() ?>participantes/listado">Listado</a>
						<a class="dropdown-item" href="<?php echo base_url() ?>participantes/guardar">Crear</a>
						</div>
                    </li>
				    <?php }?>
				</ul>
				<?php if($this->session->userdata("login")){?>
					<a href="<?php echo base_url() ?>autenticacion/logout" class="btn btn-secondary">Cerrar sesión</a>
				<?php }else{?>
					<a href="<?php echo base_url() ?>autenticacion" class="btn btn-secondary">Logear</a>
				<?php }?>
			</div>
		</nav>
	</header>

	<div class="container-fluid"><h1 class="display-4">Formulario</h1>
		<br>
		<a href="<?php echo base_url() ?>participantes/listado" class="btn btn-success">Regresar</a>
		<br>
	</div>

	<div class="container-fluid">
    <h1 class="display-4">Detalle</h1>
	<b>Nombre: </b><?php echo $nombre?></br>
	<b>Tipo: </b><?php echo $tipo?></br>
	<b>Lugar: </b><?php echo $lugar?></br>
	<b>Detalle: </b><?php echo $detalle?></br>
	<b>Fecha de inicio: </b><?php echo $fecha_inicio?></br>
	<b>Fecha de fin: </b><?php echo $fecha_fin?></br>
	<b>Hora de inicio: </b><?php echo $hora_inicio?></br>
	<b>Hora de fin: </b><?php echo $hora_fin?></br>
	</div>
	<div class="container-fluid">
    <h1 class="display-10">Participantes</h1>
    <br>
	
    <a href="guardar" class="btn btn-success">Guardar</a>
    <br>
    </div>
	
</body>
</html>