<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org">
<head th:fragment="head">
<meta charset="UTF-8" />
<title> Documento sin titulo</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</head>
<body>
	<header >
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<a class="navbar-brand" href="#">Creditos Extracurriculares</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarNav" aria-controls="navbarNav"
				aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav nav-pills mr-auto">   
				    <?php if($this->session->userdata("login")){?>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Evento</a>
						<div class="dropdown-menu" aria-labelledby="dropdown01">
						<a class="dropdown-item" href="<?php echo base_url() ?>eventos/listado">Listado</a>
						<a class="dropdown-item" href="<?php echo base_url() ?>eventos/guardar">Crear</a>
						</div>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Participantes</a>
						<div class="dropdown-menu" aria-labelledby="dropdown01">
						<a class="dropdown-item" href="<?php echo base_url() ?>participantes/listado">Listado</a>
						<a class="dropdown-item" href="<?php echo base_url() ?>participantes/guardar">Crear</a>
						</div>
                    </li>
				    <?php }?>
				</ul>
				<?php if($this->session->userdata("login")){?>
					<a href="<?php echo base_url() ?>autenticacion/logout" class="btn btn-secondary">Cerrar sesión</a>
				<?php }else{?>
					<a href="<?php echo base_url() ?>autenticacion" class="btn btn-secondary">Logear</a>
				<?php }?>
			</div>
		</nav>
	</header>
	
	<div class="container"><h1 class="display-4">Formulario</h1>

	</div>
	
	<div class="container">
	<?php echo validation_errors(); ?>
	<?php echo form_open(''); ?>

		<div class="form-group row ">
			<?php
			$labelClass=array(
				'class'=>'col-sm-2 col-form-label'
			);
			echo form_label('Identificacion','inputid',$labelClass);
			?>
			<div class="col-sm-6">
			<?php
			$input=array( 
				'name'=>'idparticipante',
				'type'=>'number', 
				'value'=>$id_participante, 
				'class'=>'form-control', 
				'id'=>'inputid'
			);
			?>
			</div>
			<?php
			echo form_input($input);
			?>
		</div>

		<div class="form-group row ">
			<?php
			$labelClass=array(
				'class'=>'col-sm-2 col-form-label'
			);
			echo form_label('Nombre','inputname',$labelClass);
			?>
			<div class="col-sm-6">
			<?php
			$input=array( 
				'name'=>'nombre',
				'type'=>'text',
				'value'=>$nombre, 
				'class'=>'form-control', 
				'id'=>'inputname',
				'placeholder'=>'Jhon'
			);
			?>
			</div>
			<?php
			echo form_input($input);
			?>
		</div>
		
		<div class="form-group row ">
			<?php
			$labelClass=array(
				'class'=>'col-sm-2 col-form-label'
			);
			echo form_label('Apellidos','inputapellido',$labelClass);
			?>
			<div class="col-sm-6">
			<?php
			$input=array( 
				'name'=>'apellido',
				'type'=>'text', 
				'value'=>$apellido, 
				'class'=>'form-control', 
				'id'=>'inputapellido',
				'placeholder'=>'silva'
			);
			?>
			</div>
			<?php
			echo form_input($input);
			?>
		</div>
		
		<div class="form-group row ">
			<?php
			$labelClass=array(
				'class'=>'col-sm-2 col-form-label'
			);
			echo form_label('Correo','inputcorreo',$labelClass);
			?>
			<div class="col-sm-6">
			<?php
			$input=array( 
				'name'=>'correo',
				'type'=>'email', 
				'value'=>$correo, 
				'class'=>'form-control', 
				'id'=>'inputcorreo',
				'placeholder'=>'@gmail.com'
			);
			?>
			</div>
			<?php
			echo form_input($input);
			?>
		</div>
		
		<div class="form-group row ">
			<?php
			$labelClass=array(
				'class'=>'col-6 col-md-4'
			);
			echo form_label('Estudiante','inputestudiante1',$labelClass);
			?>
			<?php
			$input1=array( 
				'name'=>'estudiante',
				'type'=>'radio', 
				'id'=>'inputestudiante1',
				'value'=>'si',
				'checked'=>TRUE
			);
			$input2=array( 
				'name'=>'estudiante',
				'type'=>'radio', 
				'id'=>'inputestudiante2',
				'value'=>'no'
			);
			?>
			<div class="col-6 col-md-1">
			<?php
			echo form_radio($input1);
			echo form_label('Si','inputestudiante1',$labelClass);
			?>
			</div>
			<div class="col-6 col-md-1">
			<?php
			echo form_radio($input2);
			echo form_label('No','inputestudiante2',$labelClass);
			?>
			</div>
		</div>
		
		


		<?php echo form_submit('mysubmit', 'Enviar',"class='btn btn-primary'");?>
		
		<a href="<?php echo base_url() ?>participantes/listado" class="btn btn-success">Regresar</a>
    <?php echo form_close(); ?>
	</div>
	
</body>
</html>