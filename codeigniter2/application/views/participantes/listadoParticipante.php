<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org">
<head th:fragment="head">
<meta charset="UTF-8" />
<title> Documento sin titulo</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</head>
<body>
	<header >
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<a class="navbar-brand" href="#">Creditos Extracurriculares</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarNav" aria-controls="navbarNav"
				aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav nav-pills mr-auto">   
				    <?php if($this->session->userdata("login")){?>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Evento</a>
						<div class="dropdown-menu" aria-labelledby="dropdown01">
						<a class="dropdown-item" href="<?php echo base_url() ?>eventos/listado">Listado</a>
						<a class="dropdown-item" href="<?php echo base_url() ?>eventos/guardar">Crear</a>
						</div>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Participantes</a>
						<div class="dropdown-menu" aria-labelledby="dropdown01">
						<a class="dropdown-item" href="<?php echo base_url() ?>participantes/listado">Listado</a>
						<a class="dropdown-item" href="<?php echo base_url() ?>participantes/guardar">Crear</a>
						</div>
                    </li>
				    <?php }?>
				</ul>
				<?php if($this->session->userdata("login")){?>
					<a href="<?php echo base_url() ?>autenticacion/logout" class="btn btn-secondary">Cerrar sesión</a>
				<?php }else{?>
					<a href="<?php echo base_url() ?>autenticacion" class="btn btn-secondary">Logear</a>
				<?php }?>
			</div>
		</nav>
	</header>
	<div class="container">
    <h1 class="display-4">Participantes</h1>
	
	<form method="get" action="<?php echo base_url() ?>participantes/buscar_listado">
	<div class="input-group mt-2 mb-2">
		<input name="nombre" type="text" value="<?php echo $nombre ?>" class="form-control" placeholder="Buscar">
			<div class="input-group-append">
			<button class="btn btn-outline-secondary" type="submit">Enviar</button>
			</div>
		</div>
	</form>
	
    <br>
    <table class="table">
			<thead>
				<th scope="col">#</th>
				<th scope="col">Nombre</th>
				<th scope="col">Apellido</th>
				<th scope="col">correo</th>
				<th scope="col">estudiante</th>
				<th scope="col">Operacion</th>
            </thead>
            <thbody>
                <?php foreach ($participantes as $key => $e) :?>
                    <tr>
                        <th scope="row"><?php echo $e->id_participante?></th>
                        <td><?php echo $e->nombre?></td>
                        <td><?php echo $e->apellido?></td>
                        <td><?php echo $e->correo?></td>
                        <td><?php echo $e->estudiante?></td>
                        <td>
                            <button type="button" class="btn btn-outline-dark"><a href="<?php echo base_url() ?>participantes/guardar/<?php echo $e->id_participante?>">Editar  </button>
                            <?php if($permisos->eliminar == 1){?>
							<button type="button" class="btn btn-outline-dark"><a href="" 
							 data-toggle="modal" data-target="#deleteParticipante" data-id="<?php echo $e->id_participante?>"
							data-nombre="<?php echo $e->nombre?>">Borrar  </button>
							<?php }?>
						</td>

                <?php endforeach; ?>

			
    </table>
	<nav aria-label="Page navigation example">
		<ul class="pagination">
			<?php
			$anterior=$primerpagina-1; 
			$siguiente=$primerpagina+1; 
			if($anterior<=0){ $prev=1;} 
			if($siguiente>$finalpagina){ $siguiente=$finalpagina;}
			?>
			<li class="page-item"><a class="page-link" href="<?php echo base_url()."participantes/listado/".$anterior ?>?nombre=<?php echo $nombre ?>">Previous</a></li>
			
			<?php
			 	for($i=1;$i<=$finalpagina;$i++){?>
					<li class="page-item"><a class="page-link" href="<?php echo base_url()."participantes/listado/".$i ?>?nombre=<?php echo $nombre ?>"><?php echo $i ?></a></li>
			<?php	 }
			?>
			<li class="page-item"><a class="page-link" href="<?php echo base_url()."participantes/listado/".$siguiente ?>?nombre=<?php echo $nombre ?>">Next</a></li>
		</ul>
	</nav>
    </div>

	<div class="modal fade" id="deleteParticipante" tabindex="-1" role="dialog" aria-labelledby="deleteParticipanteLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="deleteParticipanteLabel">
					Vas a borrar el evento: 
					<span></span>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn btn-primary" id="b-borrar">Borrar</button>
			</div>
			</div>
		</div>
	</div>
	
	<script>
	var id;
	var link;
		$('#deleteParticipante').on('show.bs.modal', function (event) {
		link = $(event.relatedTarget) // Button that triggered the modal
		id = link.data('id') // Extract info from data-* attributes
		var nombre=link.data('nombre')
		// If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
		// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
		var modal = $(this)
		modal.find('.modal-title span').text(nombre)
		})

		$("#b-borrar").click(function(){
			$.ajax({
				url: "<?php echo base_url()?>participantes/borrar/"+id,
				context: document.body
				}).done(function(res) {
					console.log(res)
					$("#deleteParticipante").modal('hide')
					$(link).parent().parent().parent().remove()
					location.reload()
				});
		});

	</script>

</body>
</html>