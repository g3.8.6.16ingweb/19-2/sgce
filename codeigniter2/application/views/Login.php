<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org">
<head th:fragment="head">
<meta charset="UTF-8" />
<title> Documento sin titulo</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body >

	<div class="container text-center col-3">
		<img class="mb-4 " src="https://1.bp.blogspot.com/-3wALNMake70/XK-07VtIngI/AAAAAAABOrY/n3X_ZJV5fGEpTs8ppMQvKk_yic7BfyBYQCLcBGAs/s1600/universidad-la-salle-logo.jpg" alt="" width="160" height="163">
    
    <form class="form-signin" method="post" action="<?php echo base_url() ?>autenticacion/login">
      <h1 class="h3 mb-3 font-weight-normal col-sm-12"></h1>
      <label for="inputEmail" class="sr-only">Usuario</label>
      <input type="text"  class="form-control" placeholder="Email address" name="username" required autofocus>
      <label for="inputPassword" class="sr-only">Contraseña</label>
      <input type="password" id="inputPassword" class="form-control" name="password" placeholder="Password" required>
      
      <button class="btn btn-lg btn-primary btn-block" type="submit">Iniciar sesión</button>
      <p class="mt-5 mb-3 text-muted">&copy; 2011-2019</p>
    </form>
    </div>
	
</body>
</html>