<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org">
<head th:fragment="head">
<meta charset="UTF-8" />
<title> Documento sin titulo</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</head>
<body>
	<header >
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<a class="navbar-brand" href="#">Creditos Extracurriculares</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarNav" aria-controls="navbarNav"
				aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav nav-pills mr-auto">   
				    <?php if($this->session->userdata("login")){?>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Evento</a>
						<div class="dropdown-menu" aria-labelledby="dropdown01">
						<a class="dropdown-item" href="<?php echo base_url() ?>eventos/listado">Listado</a>
						<a class="dropdown-item" href="<?php echo base_url() ?>eventos/guardar">Crear</a>
						</div>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Participantes</a>
						<div class="dropdown-menu" aria-labelledby="dropdown01">
						<a class="dropdown-item" href="<?php echo base_url() ?>participantes/listado">Listado</a>
						<a class="dropdown-item" href="<?php echo base_url() ?>participantes/guardar">Crear</a>
						</div>
                    </li>
				    <?php }?>
				</ul>
				<?php if($this->session->userdata("login")){?>
					<a href="<?php echo base_url() ?>autenticacion/logout" class="btn btn-secondary">Cerrar sesión</a>
				<?php }else{?>
					<a href="<?php echo base_url() ?>autenticacion" class="btn btn-secondary">Logear</a>
				<?php }?>
			</div>
		</nav>
	</header>
	<script>
		function validar(){
	var  fechainicio, fechafin, horainicio, horafin;
	fechainicio = document.getElementById("inputfechainicio").value;
	fechafin = document.getElementById("inputfechafin").value;
	horainicio = document.getElementById("inputhorainicio").value;
	horafin = document.getElementById("inputhorafin").value;

	if(fechainicio>fechafin){
		alert("La fecha de incio y fin incorrectos");
		return false;
	}
	if(horainicio>horafin){
		alert("La hora de incio y fin incorrectos");
		return false;
	}}
	</script>
	<?php $data = array('onsubmit' => "return validar();"); ?>
	<div class="container"><h1 class="display-4">Formulario</h1>
	<div class="container">
	<?php echo validation_errors(); ?>
	<?php echo form_open('', $data); ?>
		<div class="form-group row ">
			<?php
			$labelClass=array(
				'class'=>'col-sm-2 col-form-label'
			);
			echo form_label('Nombre','inputname',$labelClass);
			?>
			<div class="col-sm-6">
			<?php
			$input=array( 
				'name'=>'nombre',
				'type'=>'text',
				'value'=>$nombre, 
				'class'=>'form-control', 
				'id'=>'inputname',
				'placeholder'=>'Jhon'
			);
			?>
			</div>
			<?php
			echo form_input($input);
			?>
		</div>
		
		<div class="form-group row ">
			<?php
			$labelClass=array(
				'class'=>'col-sm-2 col-form-label'
			);
			echo form_label('Tipo','inputtipo',$labelClass);
			?>
			<div class="col-sm-6">
			<?php
			$input=array( 
				'name'=>'tipo',
				'type'=>'text', 
				'value'=>$tipo, 
				'class'=>'form-control', 
				'id'=>'inputtipo',
				'placeholder'=>'charla'
			);
			?>
			</div>
			<?php
			echo form_input($input);
			?>
		</div>
		
		<div class="form-group row ">
			<?php
			$labelClass=array(
				'class'=>'col-sm-2 col-form-label'
			);
			echo form_label('Lugar','inputlugar',$labelClass);
			?>
			<div class="col-sm-6">
			<?php
			$input=array( 
				'name'=>'lugar',
				'type'=>'text', 
				'value'=>$lugar, 
				'class'=>'form-control', 
				'id'=>'inputlugar',
				'placeholder'=>'Av. La Salle'
			);
			?>
			</div>
			<?php
			echo form_input($input);
			?>
		</div>
		
		<div class="form-group row ">
			<?php
			$labelClass=array(
				'class'=>'col-sm-2 col-form-label'
			);
			echo form_label('Detalle','inputdetalle',$labelClass);
			?>
			<div class="col-sm-6">
			<?php
			$input=array( 
				'name'=>'detalle',
				'type'=>'text', 
				'value'=>$detalle, 
				'class'=>'form-control', 
				'id'=>'inputdetalle',
				'placeholder'=>'...'
			);
			?>
			</div>
			<?php
			echo form_input($input);
			?>
		</div>
		
		<div class="form-group row ">
			<?php
			$labelClass=array(
				'class'=>'col-sm-2 col-form-label'
			);
			echo form_label('Fecha de inicio','inputfechainicio',$labelClass);
			?>
			<div class="col-sm-6">
			<?php
			$input=array( 
				'name'=>'fechaInicio',
				'type'=>'date', 
				'value'=>$fecha_inicio, 
				'class'=>'form-control', 
				'id'=>'inputfechainicio'
			);
			?>
			</div>
			<?php
			echo form_input($input);
			?>
		</div>
		
		<div class="form-group row ">
			<?php
			$labelClass=array(
				'class'=>'col-sm-2 col-form-label'
			);
			echo form_label('Fecha de fin','inputfechafin',$labelClass);
			?>
			<div class="col-sm-6">
			<?php
			$input=array( 
				'name'=>'fechaFin',
				'type'=>'date', 
				'value'=>$fecha_fin, 
				'class'=>'form-control', 
				'id'=>'inputfechafin'
			);
			?>
			</div>
			<?php
			echo form_input($input);
			?>
		</div>
		
		<div class="form-group row ">
			<?php
			$labelClass=array(
				'class'=>'col-sm-2 col-form-label'
			);
			echo form_label('Hora de inicio','inputhorainicio',$labelClass);
			?>
			<div class="col-sm-6">
			<?php
			$input=array( 
				'name'=>'horaInicio',
				'type'=>'time', 
				'value'=>$hora_inicio, 
				'class'=>'form-control', 
				'id'=>'inputhorainicio'
			);
			?>
			</div>
			<?php
			echo form_input($input);
			?>
		</div>
		
		<div class="form-group row ">
			<?php
			$labelClass=array(
				'class'=>'col-sm-2 col-form-label'
			);
			echo form_label('Hora de fin','inputhorafin',$labelClass);
			?>
			<div class="col-sm-6">
			<?php
			$input=array( 
				'name'=>'horaFin',
				'type'=>'time', 
				'value'=>$hora_fin, 
				'class'=>'form-control', 
				'id'=>'inputhorafin'
			);
			?>
			</div>
			<?php
			echo form_input($input);
			?>
		</div>

		<?php echo form_submit('mysubmit', 'Enviar',"class='btn btn-primary'");?>
		
		<a href="<?php echo base_url() ?>eventos/listado" class="btn btn-success">Regresar</a>
    <?php echo form_close(); ?>
	</div>
	
</body>
</html>