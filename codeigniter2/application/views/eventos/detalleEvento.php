<!DOCTYPE html>
<html xmlns:th="http://www.thymeleaf.org">
<head th:fragment="head">
<meta charset="UTF-8" />
<title> Documento sin titulo</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</head>
<body>
	<header >
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
			<a class="navbar-brand" href="#">Creditos Extracurriculares</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarNav" aria-controls="navbarNav"
				aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav nav-pills mr-auto">   
				    <?php if($this->session->userdata("login")){?>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Evento</a>
						<div class="dropdown-menu" aria-labelledby="dropdown01">
						<a class="dropdown-item" href="<?php echo base_url() ?>eventos/listado">Listado</a>
						<a class="dropdown-item" href="<?php echo base_url() ?>eventos/guardar">Crear</a>
						</div>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Participantes</a>
						<div class="dropdown-menu" aria-labelledby="dropdown01">
						<a class="dropdown-item" href="<?php echo base_url() ?>participantes/listado">Listado</a>
						<a class="dropdown-item" href="<?php echo base_url() ?>participantes/guardar">Crear</a>
						</div>
                    </li>
				    <?php }?>
				</ul>
				<?php if($this->session->userdata("login")){?>
					<a href="<?php echo base_url() ?>autenticacion/logout" class="btn btn-secondary">Cerrar sesión</a>
				<?php }else{?>
					<a href="<?php echo base_url() ?>autenticacion" class="btn btn-secondary">Logear</a>
				<?php }?>
			</div>
		</nav>
	</header>

	<div class="container-fluid">
	<nav class="navbar navbar-expand-lg navbar-light bg-light rounded">

		<div class="collapse navbar-collapse" id="navbarsExample09">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item">
			<a class="nav-link" href="" 
			data-toggle="modal" data-target="#AgregarParticipante" data-id="<?php echo $id?>">Agregar</a>
			</li>
		</ul>
			<a class="nav-link" href="<?php echo base_url() ?>eventos/listado">Regresar</a>
		</div>
	</nav>
	</div>

	<div class="container">
	

    <h1 class="display-4">Detalle</h1>
		<br>
	<b>Nombre: </b><?php echo $nombre?></br>
	<b>Tipo: </b><?php echo $tipo?></br>
	<b>Lugar: </b><?php echo $lugar?></br>
	<b>Detalle: </b><?php echo $detalle?></br>
	<b>Fecha de inicio: </b><?php echo $fecha_inicio?></br>
	<b>Fecha de fin: </b><?php echo $fecha_fin?></br>
	<b>Hora de inicio: </b><?php echo $hora_inicio?></br>
	<b>Hora de fin: </b><?php echo $hora_fin?></br>
	</div>
	<div class="container">
    <h1 class="display-10">Participantes</h1>
	<div class="mb-2">
	</div>
	<table class="table">
			<thead>
				<th scope="col">Nombre</th>
				<th scope="col">Apellido</th>
				<th scope="col">estudiante</th>
				<th scope="col">Operacion</th>
            </thead>
            <thbody>
                <?php foreach ($lista_participante as $key => $e) :?>
                    <tr>
                        <td><?php echo $e->nombre?></td>
                        <td><?php echo $e->apellido?></td>
                        <td><?php echo $e->estudiante?></td>
                        <td>
                            <button type="button" class="btn btn-outline-dark"><a href="<?php echo base_url() ?>participantes/borrar/<?php echo $e->id_participante?>">Borrar  </button>
                        </td>

                <?php endforeach; ?>

			
    </table>


	<div class="modal fade " id="AgregarParticipante" tabindex="-1" role="dialog" aria-labelledby="AgregarParticipanteLabel" aria-hidden="true">
	<div class="modal-dialog " role="document">
		<div class="modal-content">
		<div class="modal-header">
		</div>
		<div class="modal-body">
    	
			<form  method="post" action="<?php echo base_url() ?>asistencias/insertar">
				<label for="recipient-name" class="col-form-label">Id del participante:</label>
				<input type="number" name="id_participante" value="" min="10000000" max="99999999">
				<input type="hidden" name="id_evento" value="<?php echo $id?>">
				<input type="hidden" name="creditos" value="0.25">
				<button class="btn btn-outline-dark" type="submit" id="b-borrar">Agregar</button>
			</form>
                        
		</div>
		<div class="modal-footer">
		</div>
		</div>
	</div>
	</div>
	<script>

	</script>
	<script>
	
	$('#AgregarParticipante').on('show.bs.modal', function (event) {
	var button = $(event.relatedTarget) // Button that triggered the modal
	var recipient = button.data('whatever') // Extract info from data-* attributes
	// If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
	// Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
	var modal = $(this)
	})

		$("#b-borrar").click(function(){
			$.ajax({
				url: "",
				context: document.body
				}).done(function(res) {
					console.log(res)
					//$("#AgregarParticipante").reload()
					//$("#AgregarParticipante").modal('hide')
					//location.reload()
					//$("#AgregarParticipante").modal("show")
				});
		});


	</script>

    </div>
	
</body>
</html>