<?php

class evento extends CI_Model {

    public $table ='evento';
    public $table_id ='id_evento';

    public function __construct(){

    }

    function find($id){
        $this->db->select();
        $this->db->from($this->table);
        $this->db->where($this->table_id,$id);

        $query = $this->db->get();
        return $query->row();
    }

    function findAll(){
        $this->db->select();
        $this->db->from($this->table);

        $query = $this->db->get();
        return $query->result();
    }

    function insert($data){
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    function update($id,$data){
        $this->db->where($this->table_id,$id);
        $this->db->update($this->table, $data);
    }

    function delete($id){
        $this->db->where($this->table_id,$id);
        $this->db->delete($this->table);
    }

    function count($nombre){
        $this->db->select();
        $this->db->from($this->table);
        $this->db->like("nombre",$nombre);

        $query = $this->db->get();
        return $query->num_rows();
    }
    

    function pagination($pag_size,$offset,$nombre){
        $this->db->select();
        $this->db->from($this->table);
        $this->db->limit($pag_size,$offset);
        $this->db->like("nombre",$nombre);

        $query = $this->db->get();
        return $query->result();
    }

/////////////////////////////////////
    
    function findAs($id){
        $this->db->select('asistencia.id_asistencia, participante.id_participante, participante.nombre, participante.apellido, participante.estudiante');
        $this->db->from($this->table);
        $this->db->join('asistencia', 'evento.id_evento = asistencia.id_evento', 'inner');
        $this->db->join('participante', 'participante.id_participante = asistencia.id_participante', 'inner');
        $this->db->where('evento.id_evento',$id);
        $query = $this->db->get();
        return $query->result();
    }


}