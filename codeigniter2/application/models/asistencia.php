<?php

class asistencia extends CI_Model {
    public $table ='asistencia';
    public $table_id ='id_asistencia';
    
    function delete($id){
        $this->db->where('id_asistencia',$id);
        $this->db->delete('asistencia');
    }

    function insert($data){
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }
    
    function find($id,$id2){
        $this->db->select();
        $this->db->from($this->table);
        $this->db->where('id_participante',$id);
        $this->db->where('id_evento',$id2);

        $query = $this->db->get();
        return $query->row();
    }
}
