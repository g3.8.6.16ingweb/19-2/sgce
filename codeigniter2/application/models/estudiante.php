<?php

class estudiante extends CI_Model {

    public $table ='estudiante';
    public $table_id ='id_estudiante';

    public function __construct(){

    }

    function find($id){
        $this->db->select();
        $this->db->from($this->table);
        $this->db->where($this->table_id,$id);

        $query = $this->db->get();
        return $query->row();
    }
    function update($id,$data){
        $this->db->where($this->table_id,$id);
        $this->db->update($this->table, $data);
    }
}